package main;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import testencrypt.Encriptar;
import java.io.*;


public class Main {
    static BufferedReader read = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String arg[]) throws IOException
    {
        ApplicationContext context = new ClassPathXmlApplicationContext("file:META-INF/beans.xml");
        BeanFactory factory = context;
        Encriptar test = (Encriptar) factory.getBean("Encriptar");

        System.out.println("Ingrese el texto a encriptar");
        String texto = read.readLine();

        test.run(texto);
    }
}
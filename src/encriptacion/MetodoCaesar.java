package encriptacion;

public class MetodoCaesar implements ENCRIPTA{
    @Override
    public String encripta(String texto) {
        int s = 1;

        StringBuffer resultado= new StringBuffer();

        for (int i=0; i<texto.length(); i++)
        {
            if (Character.isUpperCase(texto.charAt(i)))
            {
                char ch = (char)(((int)texto.charAt(i) +
                        s - 65) % 26 + 65);
                resultado.append(ch);
            }
            else
            {
                char ch = (char)(((int)texto.charAt(i) +
                        s - 97) % 26 + 97);
                resultado.append(ch);
            }
        }
        
        return resultado.toString();
    }

    @Override
    public String desencripta(String texto) {
        int s = (-1);

        StringBuffer resultado= new StringBuffer();

        for (int i=0; i<texto.length(); i++)
        {
            if (Character.isUpperCase(texto.charAt(i)))
            {
                char ch = (char)(((int)texto.charAt(i) +
                        s - 65) % 26 + 65);
                resultado.append(ch);
            }
            else
            {
                char ch = (char)(((int)texto.charAt(i) +
                        s - 97) % 26 + 97);
                resultado.append(ch);
            }
        }

        return resultado.toString();
    }
}

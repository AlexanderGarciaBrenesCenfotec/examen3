package encriptacion;

public interface ENCRIPTA {
    public String encripta(String texto);
    public String desencripta(String texto);
}

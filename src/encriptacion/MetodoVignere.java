package encriptacion;

import java.util.Locale;

public class MetodoVignere implements ENCRIPTA{
    @Override
    public String encripta(String texto) {
        String keyword = "luffy";

        texto = texto.toUpperCase(Locale.ROOT);
        keyword = keyword.toUpperCase(Locale.ROOT);
//        encryptDecrypt(plaintext,keyword);

        char msg[] = texto.toCharArray();
        int msgLen = msg.length;
        int i,j;

        char key[] = new char[msgLen];
        char encryptedMsg[] = new char[msgLen];

        for(i = 0, j = 0; i < msgLen; ++i, ++j)
        {
            if(j == keyword.length())
            {
                j = 0;
            }
            key[i] = keyword.charAt(j);
        }

        //encryption code
        for(i = 0; i < msgLen; ++i)
            encryptedMsg[i] = (char) (((msg[i] + key[i]) % 26) + 'A');

        return String.valueOf(encryptedMsg);
    }

    @Override
    public String desencripta(String texto) {
        String keyword = "luffy";

        texto = texto.toUpperCase(Locale.ROOT);
        keyword = keyword.toUpperCase(Locale.ROOT);

        char msg[] = texto.toCharArray();
        int msgLen = msg.length;
        int i,j;

        char key[] = new char[msgLen];
        char decryptedMsg[] = new char[msgLen];

        for(i = 0, j = 0; i < msgLen; ++i, ++j)
        {
            if(j == keyword.length())
            {
                j = 0;
            }
            key[i] = keyword.charAt(j);
        }

        for(i = 0; i < msgLen; ++i)
            decryptedMsg[i] = (char)((((msg[i] - key[i]) + 26) % 26) + 'A');

        return String.valueOf(decryptedMsg);
    }
}

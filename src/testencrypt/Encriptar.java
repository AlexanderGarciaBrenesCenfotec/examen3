package testencrypt;

import encriptacion.ENCRIPTA;
import org.springframework.beans.factory.annotation.Autowired;
//import encriptacion.MetodoCaesar;
//import encriptacion.MetodoVignere;


public class Encriptar {
    private ENCRIPTA encripta;

    @Autowired
    public void setEncripta (ENCRIPTA pEncripta){
        this.encripta = pEncripta;
    }

    public void run(String texto){
        String textoEncriptado = encripta.encripta(texto);
        System.out.println("Texto encriptado: " + textoEncriptado);
        System.out.println("Texto desencriptado: " + encripta.desencripta(textoEncriptado));
    }
}
